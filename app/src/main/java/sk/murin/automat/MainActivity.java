package sk.murin.automat;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.text.InputType;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

public class MainActivity extends AppCompatActivity {
//https://gitlab.com/toofreshtobesmashed/automat-android.git
    private final ImageView[][] imageViews = new ImageView[3][3];
    private final Random r = new Random();
    private double sumInDouble;
    private double moneyRate;
    private static final int[] fruits = {R.drawable.tentobanan, R.drawable.tentoorange, R.drawable.tentoraspberry};
    private Button btnPlay;
    private TextView tvMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imageViews[0][0] = findViewById(R.id.img1);
        imageViews[0][1] = findViewById(R.id.img2);
        imageViews[0][2] = findViewById(R.id.img3);
        imageViews[1][0] = findViewById(R.id.img4);
        imageViews[1][1] = findViewById(R.id.img5);
        imageViews[1][2] = findViewById(R.id.img6);
        imageViews[2][0] = findViewById(R.id.img7);
        imageViews[2][1] = findViewById(R.id.img8);
        imageViews[2][2] = findViewById(R.id.img9);

        tvMessage = findViewById(R.id.tv_message);
        btnPlay = findViewById(R.id.btn_play);

        btnPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showIcon();
                setMessage();
            }
        });
        insertMoneyy();
    }

    private void showIcon() {
        int randomIcon = r.nextInt(fruits.length);
        int positionY = r.nextInt(3);
        int positionX = r.nextInt(3);

        if (String.valueOf(randomIcon).equals(imageViews[positionY][positionX].getContentDescription().toString())) {
            randomIcon++;
            randomIcon = randomIcon % fruits.length;
        }


        imageViews[positionY][positionX].setImageResource(fruits[randomIcon]);
        imageViews[positionY][positionX].setContentDescription(String.valueOf(randomIcon));
    }

    public boolean checkRows() {

        for (int i = 0; i < 3; i++) {
            if (imageViews[i][0].getContentDescription() != null && imageViews[i][0].getContentDescription().equals(imageViews[i][1].getContentDescription())
                    && imageViews[i][1].getContentDescription().equals(imageViews[i][2].getContentDescription())) {
                imageViews[i][0].setImageBitmap(null);
                imageViews[i][1].setImageBitmap(null);
                imageViews[i][2].setImageBitmap(null);
                imageViews[i][0].setContentDescription("4");
                imageViews[i][1].setContentDescription("5");
                imageViews[i][2].setContentDescription("6");
                return true;
            }
        }
        return false;
    }

    public void insertMoneyy() {
        AlertDialog dialog = new AlertDialog.Builder(this)
                .setView(R.layout.dialog).setCancelable(false).show();
        dialog.findViewById(R.id.btn_submit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText editTextMoney = dialog.findViewById(R.id.edit_Text_money);
                String money = editTextMoney.getText().toString();
                if (money.isEmpty()) {
                    return;
                }
                sumInDouble = Double.parseDouble(money);
                if (sumInDouble < 10) {
                    Toast.makeText(getApplicationContext(), "Minimalny vklad je 10€", Toast.LENGTH_LONG).show();
                    return;
                }
                dialog.dismiss();

                tvMessage.setText("Máš " + sumInDouble + "€ stávkuješ za " + moneyRate + "€");
                insertRate();
            }
        });
    }

    public void insertRate() {
        AlertDialog dialog = new AlertDialog.Builder(this)
                .setView(R.layout.dialog).setCancelable(false).show();
        TextView textView = dialog.findViewById(R.id.textViewMsg);
        textView.setText("Za kolko eur chces stavkovat?");
        dialog.findViewById(R.id.btn_submit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText editTextRate = dialog.findViewById(R.id.edit_Text_money);
                String rate = editTextRate.getText().toString();
                if (rate.isEmpty()) {
                    return;
                }
                moneyRate = Double.parseDouble(rate);
                if (moneyRate > sumInDouble) {
                    Toast.makeText(getApplicationContext(), "Nemoze byt stavka vacsia ako vklad", Toast.LENGTH_LONG).show();
                    return;
                }
                dialog.dismiss();
                tvMessage.setText("Máš " + sumInDouble + "€ stávkuješ za " + moneyRate + "€");

            }
        });
    }

    public void setMessage() {
        if (checkRows()) {
            double zaloha = sumInDouble;
            sumInDouble += moneyRate * 9;
            Toast.makeText(getApplicationContext(), "Pripocitalo sa " + (sumInDouble - zaloha) + " €", Toast.LENGTH_LONG).show();
        } else {
            sumInDouble -= moneyRate;
        }
        tvMessage.setText("Máš " + sumInDouble + "€ stávkuješ za " + moneyRate + "€");
        if (sumInDouble <= 0) {
            sumInDouble = 0;
            Toast.makeText(this, "Prehral si vsetko", Toast.LENGTH_LONG).show();
            btnPlay.setEnabled(false);
        }
    }
}